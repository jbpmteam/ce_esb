
package org.com.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.com.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GlobalFlow_QNAME = new QName("http://service.com.org/", "globalFlow");
    private final static QName _GlobalFlowResponse_QNAME = new QName("http://service.com.org/", "globalFlowResponse");
    private final static QName _ParseException_QNAME = new QName("http://service.com.org/", "ParseException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.com.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GlobalFlow }
     * 
     */
    public GlobalFlow createGlobalFlow() {
        return new GlobalFlow();
    }

    /**
     * Create an instance of {@link GlobalFlowResponse }
     * 
     */
    public GlobalFlowResponse createGlobalFlowResponse() {
        return new GlobalFlowResponse();
    }

    /**
     * Create an instance of {@link EvaluateLabClaimResponse }
     * 
     */
    public EvaluateLabClaimResponse createEvaluateLabClaimResponse() {
        return new EvaluateLabClaimResponse();
    }

    /**
     * Create an instance of {@link ResponseHeaderData }
     * 
     */
    public ResponseHeaderData createResponseHeaderData() {
        return new ResponseHeaderData();
    }

    /**
     * Create an instance of {@link ResponseLineData }
     * 
     */
    public ResponseLineData createResponseLineData() {
        return new ResponseLineData();
    }

    /**
     * Create an instance of {@link org.com.service.EvaluateLabClaim }
     * 
     */
    public org.com.service.EvaluateLabClaim createEvaluateLabClaim() {
        return new org.com.service.EvaluateLabClaim();
    }

    /**
     * Create an instance of {@link RequestHeaderData }
     * 
     */
    public RequestHeaderData createRequestHeaderData() {
        return new RequestHeaderData();
    }

    /**
     * Create an instance of {@link RequestLineData }
     * 
     */
    public RequestLineData createRequestLineData() {
        return new RequestLineData();
    }

    /**
     * Create an instance of {@link ParseException }
     * 
     */
    public ParseException createParseException() {
        return new ParseException();
    }

    /**
     * Create an instance of {@link SecondaryCodes }
     * 
     */
    public SecondaryCodes createSecondaryCodes() {
        return new SecondaryCodes();
    }

    /**
     * Create an instance of {@link GlobalFlow.EvaluateLabClaim }
     * 
     */
    public GlobalFlow.EvaluateLabClaim createGlobalFlowEvaluateLabClaim() {
        return new GlobalFlow.EvaluateLabClaim();
    }

    /**
     * Create an instance of {@link GlobalFlowResponse.Return }
     * 
     */
    public GlobalFlowResponse.Return createGlobalFlowResponseReturn() {
        return new GlobalFlowResponse.Return();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlobalFlow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com.org/", name = "globalFlow")
    public JAXBElement<GlobalFlow> createGlobalFlow(GlobalFlow value) {
        return new JAXBElement<GlobalFlow>(_GlobalFlow_QNAME, GlobalFlow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlobalFlowResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com.org/", name = "globalFlowResponse")
    public JAXBElement<GlobalFlowResponse> createGlobalFlowResponse(GlobalFlowResponse value) {
        return new JAXBElement<GlobalFlowResponse>(_GlobalFlowResponse_QNAME, GlobalFlowResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParseException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.com.org/", name = "ParseException")
    public JAXBElement<ParseException> createParseException(ParseException value) {
        return new JAXBElement<ParseException>(_ParseException_QNAME, ParseException.class, null, value);
    }

}
