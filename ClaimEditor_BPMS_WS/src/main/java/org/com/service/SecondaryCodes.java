
package org.com.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for secondaryCodes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="secondaryCodes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="secondaryDecisionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secondaryPolicyNecessityCriteria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secondaryPolicyTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "secondaryCodes", propOrder = {
    "secondaryDecisionCode",
    "secondaryPolicyNecessityCriteria",
    "secondaryPolicyTag"
})
public class SecondaryCodes {

    protected String secondaryDecisionCode;
    protected String secondaryPolicyNecessityCriteria;
    protected String secondaryPolicyTag;

    /**
     * Gets the value of the secondaryDecisionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryDecisionCode() {
        return secondaryDecisionCode;
    }

    /**
     * Sets the value of the secondaryDecisionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryDecisionCode(String value) {
        this.secondaryDecisionCode = value;
    }

    /**
     * Gets the value of the secondaryPolicyNecessityCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryPolicyNecessityCriteria() {
        return secondaryPolicyNecessityCriteria;
    }

    /**
     * Sets the value of the secondaryPolicyNecessityCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryPolicyNecessityCriteria(String value) {
        this.secondaryPolicyNecessityCriteria = value;
    }

    /**
     * Gets the value of the secondaryPolicyTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryPolicyTag() {
        return secondaryPolicyTag;
    }

    /**
     * Sets the value of the secondaryPolicyTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryPolicyTag(String value) {
        this.secondaryPolicyTag = value;
    }

}
