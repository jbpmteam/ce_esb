
package org.com.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for globalFlowResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="globalFlowResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResponseHeader" type="{http://service.com.org/}ResponseHeaderData"/>
 *                   &lt;element name="ResponseLine" type="{http://service.com.org/}ResponseLineData" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "globalFlowResponse", propOrder = {
    "_return"
})
public class GlobalFlowResponse {

    @XmlElement(name = "return")
    protected GlobalFlowResponse.Return _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GlobalFlowResponse.Return }
     *     
     */
    public GlobalFlowResponse.Return getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GlobalFlowResponse.Return }
     *     
     */
    public void setReturn(GlobalFlowResponse.Return value) {
        this._return = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResponseHeader" type="{http://service.com.org/}ResponseHeaderData"/>
     *         &lt;element name="ResponseLine" type="{http://service.com.org/}ResponseLineData" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "responseHeader",
        "responseLine"
    })
    public static class Return {

        @XmlElement(name = "ResponseHeader", required = true)
        protected ResponseHeaderData responseHeader;
        @XmlElement(name = "ResponseLine")
        protected List<ResponseLineData> responseLine;

        /**
         * Gets the value of the responseHeader property.
         * 
         * @return
         *     possible object is
         *     {@link ResponseHeaderData }
         *     
         */
        public ResponseHeaderData getResponseHeader() {
            return responseHeader;
        }

        /**
         * Sets the value of the responseHeader property.
         * 
         * @param value
         *     allowed object is
         *     {@link ResponseHeaderData }
         *     
         */
        public void setResponseHeader(ResponseHeaderData value) {
            this.responseHeader = value;
        }

        /**
         * Gets the value of the responseLine property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the responseLine property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getResponseLine().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ResponseLineData }
         * 
         * 
         */
        public List<ResponseLineData> getResponseLine() {
            if (responseLine == null) {
                responseLine = new ArrayList<ResponseLineData>();
            }
            return this.responseLine;
        }

    }

}
