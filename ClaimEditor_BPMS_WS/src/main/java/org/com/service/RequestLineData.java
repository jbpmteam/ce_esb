
package org.com.service;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestLineData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestLineData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lineNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fromDateOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="toDateOfService" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" minOccurs="0"/>
 *         &lt;element name="healthPlanGroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="placeOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="procedureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="procedureCodeModifiers" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="units" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="renderingProviderNpi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diagnosisCodePointers" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="inNetworkIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otherClaimEditorIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paidDeniedIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestLineData", propOrder = {
    "lineNumber",
    "fromDateOfService",
    "toDateOfService",
    "healthPlanGroupId",
    "placeOfService",
    "procedureCode",
    "procedureCodeModifiers",
    "units",
    "renderingProviderNpi",
    "diagnosisCodePointers",
    "inNetworkIndicator",
    "otherClaimEditorIndicator",
    "paidDeniedIndicator"
})
@XmlRootElement(name = "RequestLine")
public class RequestLineData {

	protected int lineNumber;
    protected String fromDateOfService;
    @XmlSchemaType(name = "anySimpleType")
    protected Object toDateOfService;
    protected String healthPlanGroupId;
    protected String placeOfService;
    protected String procedureCode;
    @XmlElement(nillable = true)
    protected List<String> procedureCodeModifiers;
    protected int units;
    protected String renderingProviderNpi;
    @XmlElement(nillable = true)
    protected List<String> diagnosisCodePointers;
    protected String inNetworkIndicator;
    protected String otherClaimEditorIndicator;
    protected String paidDeniedIndicator;

    /**
     * Gets the value of the lineNumber property.
     * 
     */
    public int getLineNumber() {
        return lineNumber;
    }

    public void setProcedureCodeModifiers(List<String> procedureCodeModifiers) {
		this.procedureCodeModifiers = procedureCodeModifiers;
	}

	public void setDiagnosisCodePointers(List<String> diagnosisCodePointers) {
		this.diagnosisCodePointers = diagnosisCodePointers;
	}
    /**
     * Sets the value of the lineNumber property.
     * 
     */
    public void setLineNumber(int value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the fromDateOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromDateOfService() {
        return fromDateOfService;
    }

    /**
     * Sets the value of the fromDateOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromDateOfService(String value) {
        this.fromDateOfService = value;
    }

    /**
     * Gets the value of the toDateOfService property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getToDateOfService() {
        return toDateOfService;
    }

    /**
     * Sets the value of the toDateOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setToDateOfService(Object value) {
        this.toDateOfService = value;
    }

    /**
     * Gets the value of the healthPlanGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthPlanGroupId() {
        return healthPlanGroupId;
    }

    /**
     * Sets the value of the healthPlanGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthPlanGroupId(String value) {
        this.healthPlanGroupId = value;
    }

    /**
     * Gets the value of the placeOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceOfService() {
        return placeOfService;
    }

    /**
     * Sets the value of the placeOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceOfService(String value) {
        this.placeOfService = value;
    }

    /**
     * Gets the value of the procedureCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedureCode() {
        return procedureCode;
    }

    /**
     * Sets the value of the procedureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedureCode(String value) {
        this.procedureCode = value;
    }

    /**
     * Gets the value of the procedureCodeModifiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the procedureCodeModifiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcedureCodeModifiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getProcedureCodeModifiers() {
        if (procedureCodeModifiers == null) {
            procedureCodeModifiers = new ArrayList<String>();
        }
        return this.procedureCodeModifiers;
    }

    /**
     * Gets the value of the units property.
     * 
     */
    public int getUnits() {
        return units;
    }

    /**
     * Sets the value of the units property.
     * 
     */
    public void setUnits(int value) {
        this.units = value;
    }

    /**
     * Gets the value of the renderingProviderNpi property.
     * 
     */
    public String getRenderingProviderNpi() {
        return renderingProviderNpi;
    }

    /**
     * Sets the value of the renderingProviderNpi property.
     * 
     */
    public void setRenderingProviderNpi(String value) {
        this.renderingProviderNpi = value;
    }

    /**
     * Gets the value of the diagnosisCodePointers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the diagnosisCodePointers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDiagnosisCodePointers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDiagnosisCodePointers() {
        if (diagnosisCodePointers == null) {
            diagnosisCodePointers = new ArrayList<String>();
        }
        return this.diagnosisCodePointers;
    }

    /**
     * Gets the value of the inNetworkIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInNetworkIndicator() {
        return inNetworkIndicator;
    }

    /**
     * Sets the value of the inNetworkIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInNetworkIndicator(String value) {
        this.inNetworkIndicator = value;
    }

    /**
     * Gets the value of the otherClaimEditorIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherClaimEditorIndicator() {
        return otherClaimEditorIndicator;
    }

    /**
     * Sets the value of the otherClaimEditorIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherClaimEditorIndicator(String value) {
        this.otherClaimEditorIndicator = value;
    }

    /**
     * Gets the value of the paidDeniedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaidDeniedIndicator() {
        return paidDeniedIndicator;
    }

    /**
     * Sets the value of the paidDeniedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaidDeniedIndicator(String value) {
        this.paidDeniedIndicator = value;
    }

}
