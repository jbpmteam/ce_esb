
package org.com.service;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseLineData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseLineData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lineNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="procedureCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="approvedServiceUnitCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="adviceDecisionType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="primaryDecisionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="primaryPolicyTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="primaryPolicyNecessityCriterion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payAndEducate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="secondaryCodesData" type="{http://service.com.org/}secondaryCodes" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="priorAuthNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="priorAuthStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseLineData", propOrder = {
    "lineNumber",
    "procedureCode",
    "approvedServiceUnitCount",
    "adviceDecisionType",
    "primaryDecisionCode",
    "primaryPolicyTag",
    "primaryPolicyNecessityCriterion",
    "payAndEducate",
    "secondaryCodesData",
    "reasonCode",
    "priorAuthNumber",
    "priorAuthStatusCode"
})

@XmlRootElement(name = "ResponseLineData")
public class ResponseLineData {

    protected int lineNumber;
    @XmlElement(required = true)
    protected String procedureCode;
    protected int approvedServiceUnitCount;
    @XmlElement(required = true)
    protected String adviceDecisionType;
    @XmlElement(required = true)
    protected String primaryDecisionCode;
    protected String primaryPolicyTag;
    protected String primaryPolicyNecessityCriterion;
    @XmlElement(required = true)
    protected String payAndEducate;
    @XmlElement(nillable = true)
    protected List<SecondaryCodes> secondaryCodesData;
    @XmlElement(required = true)
    protected String reasonCode;
    @XmlElement(required = true)
    protected String priorAuthNumber;
    @XmlElement(required = true)
    protected String priorAuthStatusCode;

    /**
     * Gets the value of the lineNumber property.
     * 
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     */
    public void setLineNumber(int value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the procedureCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedureCode() {
        return procedureCode;
    }

    /**
     * Sets the value of the procedureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedureCode(String value) {
        this.procedureCode = value;
    }

    /**
     * Gets the value of the approvedServiceUnitCount property.
     * 
     */
    public int getApprovedServiceUnitCount() {
        return approvedServiceUnitCount;
    }

    /**
     * Sets the value of the approvedServiceUnitCount property.
     * 
     */
    public void setApprovedServiceUnitCount(int value) {
        this.approvedServiceUnitCount = value;
    }

    /**
     * Gets the value of the adviceDecisionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdviceDecisionType() {
        return adviceDecisionType;
    }

    /**
     * Sets the value of the adviceDecisionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdviceDecisionType(String value) {
        this.adviceDecisionType = value;
    }

    /**
     * Gets the value of the primaryDecisionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryDecisionCode() {
        return primaryDecisionCode;
    }

    /**
     * Sets the value of the primaryDecisionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryDecisionCode(String value) {
        this.primaryDecisionCode = value;
    }

    /**
     * Gets the value of the primaryPolicyTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryPolicyTag() {
        return primaryPolicyTag;
    }

    /**
     * Sets the value of the primaryPolicyTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryPolicyTag(String value) {
        this.primaryPolicyTag = value;
    }

    /**
     * Gets the value of the primaryPolicyNecessityCriterion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryPolicyNecessityCriterion() {
        return primaryPolicyNecessityCriterion;
    }

    /**
     * Sets the value of the primaryPolicyNecessityCriterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryPolicyNecessityCriterion(String value) {
        this.primaryPolicyNecessityCriterion = value;
    }

    /**
     * Gets the value of the payAndEducate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayAndEducate() {
        return payAndEducate;
    }

    /**
     * Sets the value of the payAndEducate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayAndEducate(String value) {
        this.payAndEducate = value;
    }

    /**
     * Gets the value of the secondaryCodesData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the secondaryCodesData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecondaryCodesData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecondaryCodes }
     * 
     * 
     */
    public List<SecondaryCodes> getSecondaryCodesData() {
        if (secondaryCodesData == null) {
            secondaryCodesData = new ArrayList<SecondaryCodes>();
        }
        return this.secondaryCodesData;
    }

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonCode(String value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the priorAuthNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorAuthNumber() {
        return priorAuthNumber;
    }

    /**
     * Sets the value of the priorAuthNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorAuthNumber(String value) {
        this.priorAuthNumber = value;
    }

    /**
     * Gets the value of the priorAuthStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorAuthStatusCode() {
        return priorAuthStatusCode;
    }

    /**
     * Sets the value of the priorAuthStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorAuthStatusCode(String value) {
        this.priorAuthStatusCode = value;
    }

}
