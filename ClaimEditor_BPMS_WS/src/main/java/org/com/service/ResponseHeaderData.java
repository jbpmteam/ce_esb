
package org.com.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseHeaderData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseHeaderData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ceTransactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="claimNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ceErrorCodes" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseHeaderData", propOrder = {
    "ceTransactionId",
    "claimNumber",
    "ceErrorCodes"
})
public class ResponseHeaderData {

    @XmlElement(required = true)
    protected String ceTransactionId;
    @XmlElement(required = true)
    protected String claimNumber;
    @XmlElement(type = Integer.class)
    protected List<Integer> ceErrorCodes;

    /**
     * Gets the value of the ceTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCeTransactionId() {
        return ceTransactionId;
    }

    /**
     * Sets the value of the ceTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCeTransactionId(String value) {
        this.ceTransactionId = value;
    }

    /**
     * Gets the value of the claimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimNumber() {
        return claimNumber;
    }

    /**
     * Sets the value of the claimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimNumber(String value) {
        this.claimNumber = value;
    }

    /**
     * Gets the value of the ceErrorCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ceErrorCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCeErrorCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getCeErrorCodes() {
        if (ceErrorCodes == null) {
            ceErrorCodes = new ArrayList<Integer>();
        }
        return this.ceErrorCodes;
    }

	public void setCeErrorCodes(List<Integer> ceErrorCodes) {
		this.ceErrorCodes = ceErrorCodes;
	}

}
