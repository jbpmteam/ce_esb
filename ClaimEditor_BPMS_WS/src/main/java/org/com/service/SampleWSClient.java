/**
 * 
 */
package org.com.service;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import com.avalon.lbm.common.proputil.LoadPropertiesUtility;
//import com.sun.xml.internal.ws.client.BindingProviderProperties;

public class SampleWSClient {

	public static EvaluateLabClaimResponse invokeService(RequestHeaderData reqHeader, 
														 RequestLineData reqLine){
		URL wsdlURL = null;
		System.out.println("Inside Invoke Service..");
		String bpms_endpointaddress = LoadPropertiesUtility.getPropertKeyValue("lbm.claimeditor.bpms.endpointaddress");
		System.out.println("Inside Invoke Service..bpms_endpointaddress  = "+bpms_endpointaddress);
		try {
			//wsdlURL = new URL("http://HDC3-D-447S0M2:8080/invokeGlobalFlow-ws_testing/ClaimEditMain?wsdl");//Local
			//wsdlURL = new URL("http://172.28.58.192:8080/invokeGlobalFlow-ws/ClaimEditMain?wsdl");// DEV
			wsdlURL = new URL(bpms_endpointaddress);// QA			
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		QName SERVICE_NAME = new QName("http://service.com.org/", "ClaimEditMain");
		Service service = Service.create(wsdlURL, SERVICE_NAME);
		ClaimEditMain client = service.getPort(ClaimEditMain.class);

		//Set timeout until a connection is established
	    ((BindingProvider)client).getRequestContext().put("javax.xml.ws.client.connectionTimeout", "30000");

	    //Set timeout until the response is received
	    ((BindingProvider)client).getRequestContext().put("javax.xml.ws.client.receiveTimeout", "30000");
	    
	    ((BindingProvider)client).getRequestContext().put("com.sun.xml.internal.ws.connect.timeout", "30000");
	    ((BindingProvider)client).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", "30000");
	    ((BindingProvider)client).getRequestContext().put("com.sun.xml.ws.request.timeout", "30000");
	    ((BindingProvider)client).getRequestContext().put("com.sun.xml.ws.connect.timeout", "30000");
	    
		//RequestHeaderData reqHeaderData = getRequestHeaderData(reqHeader);
		//List<RequestLineData> lineDatas = getRequestLineData(reqLine);
		List<RequestLineData> lineDatas = new ArrayList<RequestLineData>();
		lineDatas.add(reqLine);
		
		GlobalFlow.EvaluateLabClaim eVal = new GlobalFlow.EvaluateLabClaim();
		eVal.setRequestHeader(reqHeader);
		eVal.setRequestLine(lineDatas);
		GlobalFlowResponse.Return returnObj =null;
		try {
			System.out.println("Invoking the Service..");
			returnObj = client.globalFlow(eVal);
			System.out.println("Done with BPMS Service Call..");
		} catch (ParseException_Exception e) {
			e.printStackTrace();
		}
		ResponseHeaderData headerData = returnObj.getResponseHeader();
		List<ResponseLineData> lineDatas2 = returnObj.getResponseLine();
		
		EvaluateLabClaimResponse claimResponse=new EvaluateLabClaimResponse();
		claimResponse.setResponseHeader(headerData);
		claimResponse.getResponseLine().addAll(lineDatas2);
		
		return claimResponse;
	}

	public static String dispResponseMarshallingContent(EvaluateLabClaimResponse result) {
		StringWriter writer = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(result.getClass()
					.getPackage().getName());
			JAXBContext jaxbCtx = jaxbContext;
			Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.TRUE);
			System.out
					.println("******************* Response ***********************");
			marshaller.marshal(result, writer);

		} catch (JAXBException ex) {
			ex.printStackTrace();
		}
		return writer.toString();
	}
}
