
package org.com.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://service.com.org/}RequestHeaderData"/>
 *         &lt;element name="RequestLine" type="{http://service.com.org/}RequestLineData" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EvaluateLabClaim", namespace="http://service.com.org/",propOrder = {
    "requestHeader",
    "requestLine"
})
@XmlRootElement(name = "evaluateLabClaim")
public class EvaluateLabClaim {

    public void setRequestLine(List<RequestLineData> requestLine) {
		this.requestLine = requestLine;
	}

	@XmlElement(name = "RequestHeader", required = true)
    protected RequestHeaderData requestHeader;
    @XmlElement(name = "RequestLine", required = true)
    protected List<RequestLineData> requestLine;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeaderData }
     *     
     */
    public RequestHeaderData getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeaderData }
     *     
     */
    public void setRequestHeader(RequestHeaderData value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the requestLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestLineData }
     * 
     * 
     */
    public List<RequestLineData> getRequestLine() {
        if (requestLine == null) {
            requestLine = new ArrayList<RequestLineData>();
        }
        return this.requestLine;
    }

}
