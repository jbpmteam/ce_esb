package com.avalon.lbm.common.proputil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class LoadPropertiesUtility {
	
	private static final Logger logger = Logger.getLogger(LoadPropertiesUtility.class.getName());
	private static LoadPropertiesUtility esbUrlDelegate = null;
	private static Properties properties = null;
	
	/**
	 * @return the properties
	 */
	public static Properties getProperties() {
	    return properties;
	}


	/**
	 * @param properties the properties to set
	 */
	public static void setProperties(Properties properties) {
		LoadPropertiesUtility.properties = properties;
	}

	static{
		System.out.println("Props... "+properties);
		
		if(properties==null){
			esbUrlDelegate = new LoadPropertiesUtility();
			try {
				properties = esbUrlDelegate.getEsbUrls();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static LoadPropertiesUtility getEsbUrlDelegate(){
		return esbUrlDelegate;
	}
	
	public Properties getEsbUrls() throws FileNotFoundException{
	    String serverInstance = System.getProperty("serverInstance");
		logger.fine("ServerInstanceAtEsbDelegate :" +serverInstance);
		Properties props = new Properties();
		FileInputStream inStream = new FileInputStream("/osw/jboss-eap-6.4/bin/lbm-common.properties"); 
		
		 try {
		    props.load(inStream);
		} catch (IOException e) {
		    e.printStackTrace();
		}
		 return props;
	}
	
	
	public static String getPropertKeyValue(String key){
	    String value = null;
	     value = properties.getProperty(key);
	     return value;
	}
	
	public static Properties loadProperties() throws Exception{
		Properties prop = new Properties();
		InputStream in;
		try {
			in = LoadPropertiesUtility.class.getClassLoader().getResourceAsStream("/osw/jboss-eap-6.4/bin/lbm-common.properties");
			prop.load(in);
			in.close();
		}catch(Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return prop;
	}
}
