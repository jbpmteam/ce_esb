/**
 * LBM project 
 */
package com.avalon.ldv;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Puri.Har
 * @since 12/22/15 1.0
 * @version %I%, %G%
 * 
 *          Confidential and Proprietary Information of Avalon Health Services,
 *          LLC, d/b/a Avalon Healthcare Solutions. All Rights Reserved.
 */

public class LabDataResponse {
	public String returnCode = "2";
	public List<Result> resultList = new ArrayList<Result>();
	public LabDataResponse(){
		
	}

}
