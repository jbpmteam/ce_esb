package com.avalon.ldv;
/**
 * LBM project 
 */

import javax.jws.WebService;

import com.avalon.ldv.GetLabDataValuesSEI;

/**
 * @author Puri.Har
 * @since 12/22/15 1.0
 * @version %I%, %G%
 * 
 * 
 *          Confidential and Proprietary Information of Avalon Health Services,
 *          LLC, d/b/a Avalon Healthcare Solutions. All Rights Reserved.
 */

@WebService(targetNamespace = "http://wr01.benemgmt.com/", endpointInterface = "com.avalon.ldv.GetLabDataValuesSEI", portName = "GetLabDataValuesPort", serviceName = "GetLabDataValuesService")
public class GetLabDataValues implements GetLabDataValuesSEI{

	/*
	 * public static void main(String[] args) { List <String> observationIDList
	 * = new ArrayList<String> ();
	 * 
	 * GetLabDataValues r = new GetLabDataValues(); LabDataResponse ldr =
	 * r.getLabValues("a","b","c",observationIDList, "e");
	 * 
	 * //debug BTW I am expecting dates in this format OK ?
	 * System.out.println("today="+ldr.resultList.get(0).observationDateTime);
	 * System.out.println("procedure code="
	 * +ldr.resultList.get(0).procedureCode);
	 * System.out.println("LOINC="+ldr.resultList.get(0).observationList.get(0).
	 * observationID);
	 * 
	 * }
	 * 
	 * String uniqueMemberID; String searchDateFrom; String searchDateTo;
	 * String[] observationIDList; String procedureCode;
	 */

	public LabDataResponse getLabValues(String uniqueMemberID, String searchDateFrom, String searchDateTo,
			String[] observationIDList, String procedureCode) {
		// List<String> observationIDList, String procedureCode) {
		
		LabDataResponse newResponse = new LabDataResponse();
		Result newResult = new Result();
		Observation newObservation = new Observation();
		
		if (uniqueMemberID.equals("000000000000000000")){
           	//Lab Response with 1 Result and 1 Observation
			newResponse.returnCode = "3";   //change the Response Return code
			newResult.observationList.add(0, newObservation); //Add the observation to the Result
			newResponse.resultList.add(0,newResult); //Add the Result to the Response
			
		}
		else if (uniqueMemberID.equals("111111111111111111")){
			//Lab Response with 2 Results with each having 1 Observation
			newResponse.returnCode = "3"; //change the Response Return code
			newResult.observationList.add(0, newObservation); //Add the observation to the Result
			newResponse.resultList.add(0,newResult); //Add the Result to the Response
			newResponse.resultList.add(1,newResult); //Add the Result to the Response
		
		}
		else if (uniqueMemberID.equals("222222222222222222")){
			//Lab Response with 1 Results with each having 3 Observations
			newResponse.returnCode = "3"; //change the Response Return code
			newResult.observationList.add(0, newObservation); //Add the observation to the Result
			newResult.observationList.add(1, newObservation); //Add the observation to the Result
			newResult.observationList.add(2, newObservation); //Add the observation to the Result
			newResponse.resultList.add(0,newResult); //Add the Result to the Response
			
		}
		else{
			
			System.out.println("uniqueMemberID not zeroes, ones or twos !!!!! ");
		}
		
		return newResponse;
		//return new LabDataResponse();

	}

}
