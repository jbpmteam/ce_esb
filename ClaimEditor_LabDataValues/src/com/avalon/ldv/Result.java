/**
 * LBM project 
 */
package com.avalon.ldv;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Puri.Har
 * @since 12/22/15 1.0
 * @version %I%, %G%
 * 
 *          Confidential and Proprietary Information of Avalon Health Services,
 *          LLC, d/b/a Avalon Healthcare Solutions. All Rights Reserved.
 */

public class Result {
	public String procedureCode = "80401";
	public String observationDateTime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	public String observationEndDateTime;
	public List<Observation> observationList = new ArrayList<Observation>();
	public Result(){
		
	}

}
