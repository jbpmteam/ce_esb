/**
 * LBM project 
 */
package com.avalon.ldv;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Puri.Har
 * @since 12/22/15 1.0
 * @version %I%, %G%
 * 
 *          Confidential and Proprietary Information of Avalon Health Services,
 *          LLC, d/b/a Avalon Healthcare Solutions. All Rights Reserved.
 */

public class Observation {
	public String observationID = "2571-8";
	public String observationValue = "50";
	public String observationUnitsOfMeasure = "count/ml";
	public String observationAbnormalFlag = "N";
	public String obvservationDateTime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

}
