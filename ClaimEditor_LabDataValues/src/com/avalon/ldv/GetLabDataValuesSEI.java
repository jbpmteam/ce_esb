package com.avalon.ldv;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;


@WebService(name = "GetLabDataValuesSEI", targetNamespace = "http://wr01.benemgmt.com/")
public interface GetLabDataValuesSEI {
	
	@WebMethod(operationName = "getLabValues", action = "urn:GetLabValues")
	@WebResult(name = "LabDataValues")
	LabDataResponse getLabValues(@WebParam(name = "uniqueMemberID") String uniqueMemberID, @WebParam(name = "searchDateFrom") String searchDateFrom,   @WebParam(name = "searchDateTo") String searchDateTo,
			@WebParam(name = "observationIDList") String[] observationIDList, @WebParam(name = "procedureCode") String procedureCode);

}