package com.avalon.lbm.claimeditor.esb;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.processor.interceptor.DefaultTraceFormatter;
import org.apache.camel.processor.interceptor.Tracer;
import org.apache.camel.spring.javaconfig.SingleRouteCamelConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.avalon.lbm.claimeditor.esb.SQSBean;

@Configuration
public class CamelRouteConfig extends SingleRouteCamelConfiguration {
	
	private final Logger LOG = LoggerFactory.getLogger(CamelRouteConfig.class);
	
	private static final String CTRL_TRACE = "lbm.ctrl.trace";
	
	public static final int RETRY_ATTEMPTS = 3;//Integer.parseInt(System.getProperty("goep.hm.common.ctrl.retry_attempts"));
	public static final int RETRY_DELAY = 3;//Integer.parseInt(System.getProperty("goep.hm.common.ctrl.retry_delay"));
	
	@Bean
	@Override
	public RouteBuilder route() {
		return new RouteBuilder() {
			@Override
			public void configure() throws Exception {

				LOG.info("****  Configuring Camel Context for Claim Editor ****");
				
				onException(Throwable.class)
					.handled(true)
					//.maximumRedeliveries(RETRY_ATTEMPTS)
	                //.redeliveryDelay(RETRY_DELAY)
	                .log(LoggingLevel.INFO,"************ Error occured **************")
					.process(new ExceptionProcessor())
					.end();				
				
				//creating tracer object
				 Tracer tracer = new Tracer();
				 // Default trace formatter is configured where it is
				 // specify which fields are required in output
				 DefaultTraceFormatter formatter = new DefaultTraceFormatter();
				 formatter.setShowBodyType(false);
				 formatter.setShowBreadCrumb(false);
				 formatter.setShowBody(false); 
				 formatter.setShowBodyType(false);
				 formatter.setShowBreadCrumb(false);
				 formatter.setShowRouteId(true);
				 formatter.setShowBody(false);
				 tracer.setLogStackTrace(true);
				 tracer.setTraceOutExchanges(false);
				 tracer.setTraceInterceptors(false);
				 tracer.setFormatter(formatter);
				 // setting formatter inside tracer
				 tracer.setFormatter(formatter);
				 //setting tracer in context
				 getContext().addInterceptStrategy(tracer);
				 // Auditing code starts -  Enable MDC logging to get Breadcrumb ID
				 getContext().setUseMDCLogging(true);
				 //switching trace on or off
				 getContext().setTracing(Boolean.valueOf(System.getProperty(CTRL_TRACE)));
				 
				 //property added to increase exchange buffer size while reading the inbound requests
				 getContext().getProperties().put(Exchange.LOG_DEBUG_BODY_MAX_CHARS, "3500");
				 
				 from("cxf:bean:ClaimEditorEndpoint")
				 	.process(new CDSRequestProcessor())
				    .split(xpath("/cla:evaluateLabClaim/RequestLine").namespace("cla", "http://wr01.benemgmt.com/claimEditor/"),new ClaimsEditorAggregator())
				    	.process(new RequestSplitProcessor())
				        .log(LoggingLevel.INFO, "Response Message from BPMS :: ${in.body}")
				    .end()
			        .process(new Processor() {
					    public void process(Exchange exchange) throws Exception {
					        String payload = exchange.getIn().getBody(String.class);
					        String req = payload+"</ns2:evaluateLabClaimResponse>";
					        exchange.getIn().setBody(req);
					   }})
					.bean(new SQSBean(),"prepareSQSCEResponse")
					.log(LoggingLevel.INFO, "Final Response Message to BCBS :: ${in.body}");
				    
				LOG.info("****  Camel Context Configured for Claim Editor  ****");

			}
		};
	}

}
