package com.avalon.lbm.claimeditor.esb;

import java.net.UnknownHostException;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

@Component
public class SQSBean {
	private final Logger LOG = Logger.getLogger(SQSBean.class.getName());
	private static final String CE_RESMSGTYPE="CeDecisionStoreResponse";
	public void prepareSQSCEResponse(Exchange exchange)throws JAXBException,UnknownHostException {

		LOG.info(":::::::SQSBean method called prepareSQSCEResponse for Persist into SQS::::::::");
		
		String response = exchange.getIn().getBody(String.class);
		String sqsTransactionId=(String)exchange.getProperty("sqsTransactionId");
		//SimpleDateFormat time_formatter = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss a z");
		//String responseArrivalTime = time_formatter.format(System.currentTimeMillis());
		String responseArrivalTime = String.valueOf(System.currentTimeMillis());

		LOG.info("Persisting Response Into SQS*****"+response+" sqsTransactionId: "+sqsTransactionId+" responseArrivalTime::: "+responseArrivalTime);
		SQSDataHandler.sendMessageToSQS(response,sqsTransactionId,responseArrivalTime,CE_RESMSGTYPE);  
		LOG.info("Persisting Response Into SQS Completed*****");
	}

}
