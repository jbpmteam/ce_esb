package com.avalon.lbm.claimeditor.esb;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTextMessage;


public class MsgListener implements MessageListener {

	// public static void main(String[] args) {
	/*
	 * GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
	 * ctx.load("classpath:applicationContext.xml"); ctx.refresh();
	 */
	static int counter=1;
	static {
		System.out.println("Listening From Listner Static Block::");
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				"admin", "admin", ActiveMQConnection.DEFAULT_BROKER_URL);

		// Create a Connection
		
		try {
			Connection connection;
			connection = connectionFactory.createConnection();
			connection.start();
			// Create a Session
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			// Getting the queue
			Destination destination = session.createQueue("KIE.SESSION");

			// MessageConsumer is used for receiving (consuming) messages
			MessageConsumer consumer = session.createConsumer(destination);

			MsgListener asyncReceiver = new MsgListener();
			consumer.setMessageListener(asyncReceiver);

			// Here we receive the message.
			// By default this call is blocking, which means it will wait
			// for a message to arrive on the queue.
			Message message = consumer.receive();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void onMessage(Message m) {
		//TextMessage message = (TextMessage) m;
		ActiveMQTextMessage message = (ActiveMQTextMessage) m;
		try {
			System.out.println("Message received from ESB: "+message.getText());
			
			/*KieServices ks = KieServices.Factory.get();
			KieContainer kContainer = ks.getKieClasspathContainer();
			KieSession ksession = kContainer.newKieSession("ksession-process");
			Map<String, Object> params = new HashMap<String, Object>();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        ClaimRequest claimRequest =new ClaimRequest();
	        claimRequest.setHealthPlanId("401");
	        claimRequest.setProcedureCode("81435");
	        Date date2 = sdf.parse("2016-05-25"); 
	        claimRequest.setEffectiveStartDate(date2);
	    	params.put("claimReq", claimRequest);
            ksession.startProcess("org.jbpm.ExperimentalAndInvestigational", params);
            ksession.fireAllRules();	
            ksession.dispose();
            String response=claimRequest.getDecision();*/
            //invokeDestination();
			          
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void invokeDestination() throws JMSException
	{
		
		   ConnectionFactory connectionFactory=new ActiveMQConnectionFactory("admin", "admin", ActiveMQConnection.DEFAULT_BROKER_URL);
           // Create a Connection
           Connection connection = connectionFactory.createConnection();
           //connection.start();
           // Create a Session
           Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
           // Create the destination
           Destination destination = session.createQueue("KIE.RESPONSE");
           // Create a MessageProducer from the Session to the Queue
           MessageProducer producer = session.createProducer(destination);
           producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
           
           counter = counter++;
           // Create a messages
           String newResponse = "<evaluateLabClaimResponse>"
                   + "<ResponseHeader><ceTransactionId></ceTransactionId>"
                   + "<claimNumber>ABCD1234</claimNumber><ceErrorCodes></ceErrorCodes>"
                   + "</ResponseHeader>"
                   + "<ResponseLine>"
                   + "<lineNumber>2</lineNumber><procedureCode>0004M</procedureCode>"
                   + "<approvedServiceUnitCount></approvedServiceUnitCount>"
                   + "<adviceDecisionType></adviceDecisionType>"
                   + "<primaryDecisionCode>D007R</primaryDecisionCode>"
                   + "<primaryPolicyTag>DNA-based Testing for Adolescent Idiopathic Scoliosis</primaryPolicyTag>"
                   + "<primaryPolicyNecessityCriterion>1</primaryPolicyNecessityCriterion>"
                   + "<payAndEducate></payAndEducate>"
                   + "<secondaryCodesData><secondaryDecisionCode></secondaryDecisionCode>"
                  + "<secondaryPolicyTag></secondaryPolicyTag>"
                   + "<secondaryPolicyNecessityCriteria></secondaryPolicyNecessityCriteria>"
                   + "</secondaryCodesData>"
                   + "<reasonCode>"+String.valueOf(counter)+"</reasonCode>"
                   + "<priorAuthNumber></priorAuthNumber>"
                   + "<priorAuthStatusCode></priorAuthStatusCode>"
                   + "</ResponseLine>"
                   + "</evaluateLabClaimResponse>";
           //TextMessage response1 = session.createTextMessage(newResponse);
           ActiveMQTextMessage final_response = (ActiveMQTextMessage)session.createTextMessage(newResponse);
           producer.send(final_response);
           System.out.println("Message sent from MsgListener..");
          
           session.close();
           connection.close();
	}
}
