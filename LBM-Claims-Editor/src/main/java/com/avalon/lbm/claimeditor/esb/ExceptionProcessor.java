package com.avalon.lbm.claimeditor.esb;

import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.binding.soap.SoapFault;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

@Component
public class ExceptionProcessor implements Processor {
	
	private final Logger LOG = Logger.getLogger(ExceptionProcessor.class.getName());
	
	public void process(Exchange exchange) throws Exception {

    	Exception exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);	    
    	String error_Message = "";
    	
    	if (exception != null)
    	      if (exception instanceof UnsupportedOperationException) {
    	        LOG.info("Error Response has returned from CDS : Member Not Found. " + exception);
    	        exchange.getIn().setFault(true);
    	        exchange.setProperty("ErrorCode", "901");
    	        String evalResponse = "<ns2:evaluateLabClaimResponse xmlns:ns2=\"http://wr01.benemgmt.com/claimEditor/\"><ResponseHeader><claimNumber>"+exchange.getProperty("claimNumber").toString()+"</claimNumber>"
    	    			+"<ceErrorCodes>901</ceErrorCodes></ResponseHeader><ResponseLine></ResponseLine></ns2:evaluateLabClaimResponse>";
    	        exchange.getIn().setBody(evalResponse);
    	      } else if (exception instanceof SAXException) {
    	        LOG.info("Error while parsing the request : " + exception);
    	        String element = "";
    	        error_Message = exception.getMessage();
    	        QName q = new QName("Server.400.3");
    	        if (error_Message.contains("RequestHeaderData")) {
    	          element = error_Message.substring(error_Message.indexOf("#AnonType_") + 1, error_Message.indexOf("RequestHeaderData")).replace("AnonType_", "");
    	          error_Message = "Bad Element Data sent with '" + element + "' in RequestHeaderData";
    	          q = new QName("Server.400.3");
    	        } else if (error_Message.contains("RequestLineData")) {
    	          element = error_Message.substring(error_Message.indexOf("#AnonType_") + 1, error_Message.indexOf("RequestLineData")).replace("AnonType_", "");
    	          error_Message = "Bad Element Data sent with '" + element + "' in RequestLineData";
    	          q = new QName("Server.400.3");
    	        } else if (error_Message.contains("Invalid content was found")) {
    	          element = error_Message.substring(error_Message.indexOf("{") + 1, error_Message.indexOf("}"));
    	          error_Message = "Required Element '" + element + "' Not Sent";
    	          q = new QName("Server.400.1");
    	        } else {
    	          error_Message = exception.getMessage();
    	        }
    	        SoapFault soapFault = new SoapFault(error_Message, q);
    	        exchange.getIn().setBody(soapFault);
    	        exchange.getIn().setFault(true);
    	      } else {
    	        LOG.info("Technical Error Occured while Processing...Cause : " + exception.getMessage());
    	        error_Message = "Internal Server Error";
    	        QName fault_code_server = new QName("Server.500");
    	        SoapFault soapFault = new SoapFault(error_Message, fault_code_server);
    	        exchange.getIn().setBody(soapFault);
    	        exchange.getIn().setFault(true);
    	      }
	}	
}

