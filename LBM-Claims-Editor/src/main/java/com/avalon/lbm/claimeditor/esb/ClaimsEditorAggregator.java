package com.avalon.lbm.claimeditor.esb;

import java.util.logging.Logger;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.com.service.SampleWSClient;


/**
 * This is our own order aggregation strategy where we can control
 * how each splitted message should be combined. As we do not want to
 * loos any message we copy from the new to the old to preserve the
 * order lines as long we process them
 */
public class ClaimsEditorAggregator implements AggregationStrategy {
 
	private final Logger LOG = Logger.getLogger(ClaimsEditorAggregator.class.getName());
	static int counter=0;
	
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        // put order together in old exchange by adding the order from new exchange
		
        if (oldExchange == null) {
             org.com.service.EvaluateLabClaimResponse evalRes = (org.com.service.EvaluateLabClaimResponse)newExchange.getIn().getBody(Object.class);
             String firstResLine = SampleWSClient.dispResponseMarshallingContent(evalRes);
             LOG.info("Aggregate firstResLine : " + firstResLine);
             if(firstResLine.contains("</ns2:evaluateLabClaimResponse>")){
            	firstResLine = firstResLine.substring(0, firstResLine.indexOf("</ns2:evaluateLabClaimResponse>"));
            	firstResLine = firstResLine.replace("<ns2:evaluateLabClaimResponse xmlns:ns2=\"http://service.com.org/\">","<ns2:evaluateLabClaimResponse xmlns:ns2=\"http://wr01.benemgmt.com/claimEditor/\">");
             }
             newExchange.getIn().setBody(firstResLine);
            return newExchange;
        }
 
        String oldReqLine = oldExchange.getIn().getBody(String.class);

        //JAXB Unmasrhalling
        org.com.service.EvaluateLabClaimResponse evalRes = (org.com.service.EvaluateLabClaimResponse)newExchange.getIn().getBody(Object.class);
        String newResLine = SampleWSClient.dispResponseMarshallingContent(evalRes);
        
        newResLine = newResLine.substring(newResLine.indexOf("<ResponseLine>"), newResLine.indexOf("</ResponseLine>"))+"</ResponseLine>";
        
        LOG.info("Aggregate old orders: " + oldReqLine);
        LOG.info("Aggregate new order: " + newResLine);
 
        oldReqLine = oldReqLine + newResLine;
        LOG.info("Aggregated order: " + oldReqLine);
        // put combined message back on old to preserve it
       
        oldExchange.getIn().setBody(oldReqLine);
 
        // return old as this is the one that has all the orders gathered until now
        return oldExchange;
    }
}
