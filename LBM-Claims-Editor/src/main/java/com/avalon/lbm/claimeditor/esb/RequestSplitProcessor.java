package com.avalon.lbm.claimeditor.esb;

import java.io.ByteArrayInputStream;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.com.service.EvaluateLabClaimResponse;
import org.com.service.RequestHeaderData;
import org.com.service.RequestLineData;
import org.com.service.SampleWSClient;
import org.springframework.stereotype.Component;

@Component
public class RequestSplitProcessor implements Processor {
	
	private final Logger LOG = Logger.getLogger(RequestSplitProcessor.class.getName());
	
	public void process(Exchange exchange) throws Exception {
		
		LOG.info("********** In RequestSplitProcessor ********** ");
		
		JAXBContext jaxbReqHeader= JAXBContext.newInstance(RequestHeaderData.class);
		Unmarshaller h_unmarshaller = jaxbReqHeader.createUnmarshaller();
		ByteArrayInputStream inputStream = new ByteArrayInputStream(exchange.getProperty("reqHeader").toString().getBytes());
	
		RequestHeaderData reqHeaderData =  (RequestHeaderData) h_unmarshaller.unmarshal(inputStream);
		reqHeaderData.setUniqueMemberId(exchange.getProperty("uniqueMemberId").toString());
		reqHeaderData.setBusinessSectorCd(exchange.getProperty("businessSectorCd").toString());		
		reqHeaderData.setCeTransactionId(exchange.getProperty("transactionId").toString());		
		
		String payload = exchange.getIn().getBody(String.class);
		String reqLine = payload.substring(payload.indexOf("<RequestLine>"), payload.indexOf("</RequestLine>"))+"</ns2:RequestLine>";
		reqLine = reqLine.replace("<RequestLine>", "<ns2:RequestLine xmlns:ns2=\"http://service.com.org/\">");
		
		JAXBContext jaxbReqLine = JAXBContext.newInstance(RequestLineData.class);
		Unmarshaller r_unmarshaller = jaxbReqLine.createUnmarshaller();
		ByteArrayInputStream r_inputStream = new ByteArrayInputStream(reqLine.getBytes());
	
		RequestLineData reqLineData =  (RequestLineData) r_unmarshaller.unmarshal(r_inputStream);
		
		EvaluateLabClaimResponse evalRes = new EvaluateLabClaimResponse();
		evalRes = SampleWSClient.invokeService(reqHeaderData, reqLineData);
		
        exchange.getIn().setBody(evalRes);
	}
	
}

