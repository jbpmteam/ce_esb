/**
 * 
 */
package com.avalon.lbm.claimeditor.esb;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.amazonaws.util.EC2MetadataUtils;
import com.avalon.lbm.common.proputil.LoadPropertiesUtility;

import java.util.logging.Level;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import java.util.logging.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class SQSDataHandler {

	/**
	 * @param args
	 */
	public static final String DEFAULT_QUEUE_NAME = LoadPropertiesUtility.getProperties().getProperty("SQS_QUEUE_NAME");
	//SQS_QUEUE_NAME=q-dev-benemgmt-com
	public static final Region DEFAULT_REGION = Region.getRegion(Regions.US_EAST_1);
	//private static AWSCredentialsProvider credentialsProvider = new DefaultAWSCredentialsProviderChain();
	//private static final Logger log = Logger.getLogger(SQSDataHandler.class.getName());
	private static final Logger log = LoggerFactory.getLogger(SQSDataHandler.class);
	
	
	public static void sendMessageToSQS(String message,String transactionId,String reqArrivalTime,String messageType)
	{
		AmazonSQS sqsClient=null;
		try
		{
		sqsClient=new AmazonSQSClient(new InstanceProfileCredentialsProvider());	
		String endpoint = "sqs." + DEFAULT_REGION + ".amazonaws.com";
		log.info("SQSDataHandler sendMessageToSQS method got Called:::");
		
		sqsClient.setEndpoint(endpoint);
		log.info("Sending message form SQSDataHandler LBM-ESB-ClaimsEditor:::" + message);
		SendMessageRequest smr = new SendMessageRequest();
		smr.setQueueUrl(DEFAULT_QUEUE_NAME);
		Map<String, MessageAttributeValue> messageAttributes = new HashMap<String, MessageAttributeValue>();
		
		String instanceId = EC2MetadataUtils.getInstanceId();
		String privateAddress = EC2MetadataUtils.getPrivateIpAddress();
		String instanceType = EC2MetadataUtils.getInstanceType();
		
		messageAttributes.put("MessageSource", new MessageAttributeValue().withDataType("String").withStringValue(instanceId+","+privateAddress+","+instanceType)); // where do you come from ?
		messageAttributes.put("TransactionId", new MessageAttributeValue().withDataType("String").withStringValue(transactionId)); // which claim ?
		messageAttributes.put("MessageType", new MessageAttributeValue().withDataType("String").withStringValue(messageType)); // who sent the message
		messageAttributes.put("CE_timestamp", new MessageAttributeValue().withDataType("String").withStringValue(reqArrivalTime)); // CE request arrival time
		smr.setMessageAttributes(messageAttributes);

		// set message frequently during CE processing
		
		smr.setMessageBody(message);
		log.info("Before Inserting Message into SQS:::" );
		SendMessageResult result = sqsClient.sendMessage(smr);
		log.info("Sent message form SQSDataHandler LBM-ESB-ClaimsEditor:::" + result.getMessageId());
		// Close the connection. This will close the session automatically
		
		}catch (Exception e) {
			log.error("Error Occured while Inserting "+messageType+" into SQS = >>");
			e.printStackTrace();
		}finally{
			sqsClient.shutdown();
		}
	}

}