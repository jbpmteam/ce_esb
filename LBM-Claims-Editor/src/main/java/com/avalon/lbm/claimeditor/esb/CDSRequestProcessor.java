package com.avalon.lbm.claimeditor.esb;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.xerces.dom.ElementNSImpl;
import org.com.service.EvaluateLabClaim;
import org.com.service.RequestHeaderData;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.avalon.lbm.common.proputil.LoadPropertiesUtility;
import com.avalon.member.service.ClientMessageCallBack;
import com.avalon.member.service.MemberSocketWebService;
import com.avalon.member.service.client.MemberDataRequest;
import com.avalon.member.service.client.MemberDataResponse;
import com.avalon.member.service.client.MemberDemographics;
import com.avalon.member.service.client.MoreDataOptions;
import com.avalon.member.service.client.ObjectFactory;
import com.avalon.member.service.client.SearchCriteria;

@Component
public class CDSRequestProcessor implements Processor {
	
	private final Logger LOG = Logger.getLogger(CDSRequestProcessor.class.getName());
	private static final String REQ_MSGTYPE_CE="CeDecisionStoreRequest";
	private static final String REQ_MSGTYPE_CE_TRIAL="CeTrialStoreRequest";
	private String REQ_MSGTYPE=null;
	public static final String BCBS_TXN_UUID_NAME = LoadPropertiesUtility.getProperties().getProperty("bcbs.transactionUUID");
		public void process(Exchange exchange) throws Exception {
		
			LOG.info("********** In CDSRequestProcessor **********");
			
			String writer = exchange.getIn().getBody(String.class);
			
			//SOAP Request Validation
			validateXMLSchema(writer);
			
			//TransactionId/UUID Creation
			//UUID uuid = UUID.randomUUID();
			//String transactionId = uuid.toString();
			String transactionId = null;
			String bcbsUUID=null;
			String sqsTransactionId=null;

			
			//Current timestamp setting
			//SimpleDateFormat time_formatter = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss a z");
			//String requestArrivalTime = time_formatter.format(System.currentTimeMillis());
			String requestArrivalTime = String.valueOf(System.currentTimeMillis());
			
			/****///TransactionId/UUID getting form SOAP Envelop Header
			CxfPayload<SoapHeader> payload = exchange.getIn().getBody(CxfPayload.class);
			if(payload!=null){
			List<SoapHeader> headers = payload.getHeaders();
			if(headers!=null && !headers.isEmpty())
			{
			for(SoapHeader header:headers){
				ElementNSImpl elementNSImpl = (ElementNSImpl)header.getObject();
				if(elementNSImpl !=null && elementNSImpl.getLocalName()!=null && BCBS_TXN_UUID_NAME.equalsIgnoreCase(elementNSImpl.getLocalName()))
				{
					String UUIDNAME=elementNSImpl.getLocalName();
					bcbsUUID=elementNSImpl.getFirstChild().getNodeValue();
					LOG.info("********** UUID NAME From BCBS **********"+UUIDNAME);
					LOG.info("********** UUID... **********"+bcbsUUID);
				}
			}
			}
			}
			//TransactionId/UUID Creation By AVLON-LBM
			UUID uuid = UUID.randomUUID();
			transactionId = uuid.toString();
			LOG.info("********** Generated CE Transaction ID... **********"+transactionId);
			
			if(bcbsUUID!=null)
			{
				sqsTransactionId=bcbsUUID+transactionId;
				LOG.info("**********IF sqsTransactionId... **********"+sqsTransactionId);
			}else{
				sqsTransactionId=transactionId+transactionId;
				LOG.info("**********ELSE sqsTransactionId... **********"+sqsTransactionId);
			}
				
			String reqHeader = writer.substring(writer.toString().indexOf("<RequestHeader>"), writer.indexOf("</RequestHeader>"))+"</ns2:RequestHeader>";
			reqHeader = reqHeader.replace("<RequestHeader>", "<ns2:RequestHeader xmlns:ns2=\"http://service.com.org/\">");
			exchange.setProperty("reqHeader", reqHeader);
			String uniqueMemberId = "";
			String businessSectorCd = "";
			String businessSegmentCd = "";
			String masterPatientId = "";
			String healthPlanGroupId = "";
			
			JAXBContext jc = JAXBContext.newInstance(org.com.service.RequestHeaderData.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			ByteArrayInputStream inputStream = new ByteArrayInputStream(reqHeader.getBytes());
		
			org.com.service.RequestHeaderData reqHeaderData =  (org.com.service.RequestHeaderData) unmarshaller.unmarshal(inputStream);
			exchange.setProperty("claimNumber", reqHeaderData.getClaimNumber());
			
			if(reqHeaderData.getExcludeMembershipIndicator()!= null && reqHeaderData.getExcludeMembershipIndicator().equals("N")){
				//Calling MemberLabBenefits WebService
				MemberSocketWebService memberService = new MemberSocketWebService();
				//Patient patientDetails = new Patient();
				MemberDataRequest serviceRequest = new ObjectFactory().createMemberDataRequest();
				MemberDemographics memDmo = new MemberDemographics();
		        memDmo.setFirstName(reqHeaderData.getPatientFirstName());
		        memDmo.setLastName(reqHeaderData.getPatientLastName());
		        memDmo.setMiddleName("");
		        memDmo.setSuffix("");
		        memDmo.setPatientId("");
		        memDmo.setGenderCode("");
		        memDmo.setMemberId("");
		        memDmo.setDateOfBirth(reqHeaderData.getPatientDateOfBirth());

		        SearchCriteria searchCriteria = new SearchCriteria();
		        searchCriteria.setPlanCode("");
				
				searchCriteria.setIdCardNumber(reqHeaderData.getIdCardNumber());
		        //searchCriteria.setIdCardNumber("ZCS92224869");
		        searchCriteria.setHealthPlanGroupId("");
		        searchCriteria.setReasonForInquiry("C");
		        //searchCriteria.setHealthPlanId("401");
		        LOG.info("********** HealthPlanId **********"+reqHeaderData.getHealthPlanId());
		        searchCriteria.setHealthPlanId(reqHeaderData.getHealthPlanId());
		        searchCriteria.setHealthPlanIdType("PI");
		        searchCriteria.setSubscriberNumber("");
		        searchCriteria.setRpn("200");
		        searchCriteria.setProductCode("MED");
		        searchCriteria.setPlanCode("885");
		        searchCriteria.setRequestDate(reqHeaderData.getFromDateOfService());;
		        
		        
		        MoreDataOptions moreOptions = new MoreDataOptions();
		        moreOptions.setRetrieveCoverageData(false);
		        moreOptions.setRetrieveMemberList(false);
		        
		        serviceRequest.setSearchCriteria(searchCriteria);
		        serviceRequest.setMoreDataOptions(moreOptions);
		        serviceRequest.setMemberDemographics(memDmo);
		        
		        ClientMessageCallBack callBack = new ClientMessageCallBack("https://services.companiondataservices.com/MemberLabBenefits/v1/");
		        LOG.info("*****************Invoking the service for File Create Member Data... ******************************* ");
		        MemberDataResponse memberDataResponse = MemberSocketWebService.invokeService(serviceRequest);
		        
		        if(memberDataResponse == null || (memberDataResponse!=null && !memberDataResponse.getResponseCode().get(0).matches(("000|961|962|993|995")))){
		        	throw new UnsupportedOperationException();
		        }else{
		        	uniqueMemberId = memberDataResponse.getMemberDemographicsOutput().getUniqueMemberId();
		        	businessSectorCd = memberDataResponse.getBusinessSectorSegment().getBusinessSectorCd();
		        	masterPatientId = memberDataResponse.getMemberDemographicsOutput().getPatientId();
		        	businessSegmentCd = memberDataResponse.getBusinessSectorSegment().getBusinessSegmentCd();
		        	if(!reqHeaderData.getHealthPlanGroupId().equals("")){
		        		healthPlanGroupId = reqHeaderData.getHealthPlanGroupId();
		        	}else{
		        		healthPlanGroupId = memberDataResponse.getHealthPlanGroupId();
		        	}
		        }
			}
		exchange.setProperty("uniqueMemberId", uniqueMemberId);
		exchange.setProperty("businessSectorCd", businessSectorCd);
		exchange.setProperty("transactionId", transactionId);
		exchange.setProperty("sqsTransactionId", sqsTransactionId);
		
		//Start Preparing ClaimEditor's Persistance Object
		LOG.info("***************** BCBS Request... ******************************* "+writer.toString());
		//String data_persist = writer.replace("<cla:evaluateLabClaim xmlns:cla=\"http://wr01.benemgmt.com/claimEditor/\">", "<cla:evaluateLabClaim xmlns:cla=\"http://service.com.org/\">");
		String data_persist = writer.replace("http://wr01.benemgmt.com/claimEditor/", "http://service.com.org/");
		
        JAXBContext jc2 = JAXBContext.newInstance(EvaluateLabClaim.class);
		Unmarshaller unmarshaller2 = jc2.createUnmarshaller();
		ByteArrayInputStream inputStreamReq = new ByteArrayInputStream(data_persist.getBytes());
	
		EvaluateLabClaim evaluateLabClaim =  (EvaluateLabClaim) unmarshaller2.unmarshal(inputStreamReq);
		RequestHeaderData requestHeaderPersistance= evaluateLabClaim.getRequestHeader();
        RequestHeaderData requestHeaderDataPersistance=new RequestHeaderData();
        EvaluateLabClaim evallabClaimPersistance=new EvaluateLabClaim();
        requestHeaderDataPersistance.setClaimNumber(requestHeaderPersistance.getClaimNumber());
        requestHeaderDataPersistance.setCobIndicator(requestHeaderPersistance.getCobIndicator());
        requestHeaderDataPersistance.setExcludeMembershipIndicator(requestHeaderPersistance.getExcludeMembershipIndicator());
        requestHeaderDataPersistance.setFromDateOfService(requestHeaderPersistance.getFromDateOfService());
        requestHeaderDataPersistance.setHealthPlanGroupId(requestHeaderPersistance.getHealthPlanGroupId());
        requestHeaderDataPersistance.setHealthPlanId(requestHeaderPersistance.getHealthPlanId());
        requestHeaderDataPersistance.setLineOfBusiness(requestHeaderPersistance.getLineOfBusiness());
        requestHeaderDataPersistance.setNumberOfLines(requestHeaderPersistance.getNumberOfLines());
        requestHeaderDataPersistance.setOriginalClaimNumber(requestHeaderPersistance.getOriginalClaimNumber());
        requestHeaderDataPersistance.setToDateOfService(requestHeaderPersistance.getToDateOfService());
        requestHeaderDataPersistance.setHealthPlanIdType(requestHeaderPersistance.getHealthPlanIdType());
        requestHeaderDataPersistance.setTrialClaimIndicator(requestHeaderPersistance.getTrialClaimIndicator());
        requestHeaderDataPersistance.setCeTransactionId(transactionId);
        
        requestHeaderDataPersistance.setBusinessSegmentCd(businessSegmentCd);
        requestHeaderDataPersistance.setMasterPatientId(uniqueMemberId);
        requestHeaderDataPersistance.setHealthPlanGroupId(healthPlanGroupId);
        requestHeaderDataPersistance.setBusinessSectorCd(businessSectorCd);
        
        evallabClaimPersistance.setRequestHeader(requestHeaderDataPersistance);
        evallabClaimPersistance.setRequestLine(evaluateLabClaim.getRequestLine());
        
        JAXBContext jc3 = JAXBContext.newInstance(EvaluateLabClaim.class);
		Marshaller marshaller3 = jc3.createMarshaller();
		marshaller3.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		StringWriter sw = new StringWriter();
		marshaller3.marshal(evallabClaimPersistance, sw);
		String finalSQSReq = sw.toString().replace("http://service.com.org/", "http://wr01.benemgmt.com/claimEditor/");
		LOG.info("sqsTransactionId*****"+sqsTransactionId+"requestArrivalTime::"+requestArrivalTime);
		LOG.info("Persisting CE Request Into SQS*****"+finalSQSReq);
		if(requestHeaderPersistance.getTrialClaimIndicator()!=null && requestHeaderPersistance.getTrialClaimIndicator().equalsIgnoreCase("Y")){
        	REQ_MSGTYPE = REQ_MSGTYPE_CE_TRIAL;
        }else{
        	REQ_MSGTYPE = REQ_MSGTYPE_CE;
        }
		SQSDataHandler.sendMessageToSQS(finalSQSReq,sqsTransactionId,requestArrivalTime,REQ_MSGTYPE);  
		LOG.info("Persisting Request Into SQS Completed*****");
        //End Preparing ClaimEditor's Persistance Object
        
		exchange.getIn().setBody(writer);
	}
		
	public void validateXMLSchema(String xml) throws SAXException,IOException{
        
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File("/osw/jboss-eap-6.4/bin/ClaimEditor_XSD.xsd"));
            Validator validator = schema.newValidator();
            
            File temp = File.createTempFile("temp", ".tmp");

            // Delete temp file when program exits.
            temp.deleteOnExit();

            // Write to temp file
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            out.write(xml);
            out.close();
            
            validator.validate(new StreamSource(temp));
        } catch (SAXException e) {
            throw e;
        } catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
    }
}

