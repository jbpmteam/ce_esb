package com.avalon.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import com.avalon.ldv.GetLabDataValuesSEI;
import com.avalon.ldv.LabDataResponse;



/**
 * @author Cole.Battestia
 *
 */
public class GetLabDataValuesClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	  String[] objList = new String[10];
	  
	  objList[0] = "100"; 
	  objList[1] = "103"; 
	  
      JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
      factory.getInInterceptors().add(new LoggingInInterceptor());
	  factory.getOutInterceptors().add(new LoggingOutInterceptor());
	  factory.setServiceClass(GetLabDataValuesSEI.class);
	  factory.setAddress("http://localhost:8080/LabDataValues/GetLabDataValues");
	  GetLabDataValuesSEI client = (GetLabDataValuesSEI) factory.create();
	  LabDataResponse Response = new LabDataResponse();
	  LabDataResponse Response2 = new LabDataResponse();
	  Response = client.getLabValues("222222222222222222", "2016-06-12", "2016-06-13",objList, "800");
	  System.out.println("The Response code is : "+ Response.returnCode);
	  System.out.println("GetLabDataValuesClientMain Client call completed!!");
	  
      System.out.println("calling the client function: ");
      Response2 = callClient("222222222222222222", "2016-06-12", "2016-06-13",objList, "800",
    		     "http://localhost:8080/LabDataValues/GetLabDataValues");
      
      System.out.println("The Response2 code is : "+ Response2.returnCode);

	}
	
	public static LabDataResponse callClient(String uniqueMemberID, String searchDateFrom, 
			                          String searchDateTo, String[] observationIDList, 
			                          String procedureCode, String hostURL){
		
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
	    factory.getInInterceptors().add(new LoggingInInterceptor());
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.setServiceClass(GetLabDataValuesSEI.class);
		factory.setAddress(hostURL);
		GetLabDataValuesSEI client = (GetLabDataValuesSEI) factory.create();
		LabDataResponse Response = new LabDataResponse();
        Response = client.getLabValues(uniqueMemberID, searchDateFrom, searchDateTo, 
        		                       observationIDList, procedureCode);
	
	return Response;
		
	} 

}

